FROM node:12-alpine
MAINTAINER "Dan Kirkpatrick <dan@frisson.tech"

WORKDIR /usr/src/app

COPY package.json ./

RUN npm install

RUN mkdir -p /etc/webhook/certs

COPY src src

EXPOSE 3000
CMD [ "node", "src/index.js" ]
