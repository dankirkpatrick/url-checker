const https = require('https');
const fs = require('fs');
const k8s = require('@kubernetes/client-node');

const kc = new k8s.KubeConfig();
kc.loadFromDefault();

const k8sExtensionsApi = kc.makeApiClient(k8s.ExtensionsV1beta1Api);

const port = 3443;
const host = "localhost";

/**
 * Parses command-line arguments, returning an options object.
 *
 * @returns {{caCertFilename: *, baseUrls: *, privateKeyFilename: *, testRun: *}}
 */
function parseArgs() {
    let testRun = false;
    let webserverPrivateKeyFilename = process.env.WEBSERVER_PRIVATE_KEY_FILENAME? process.env.WEBSERVER_PRIVATE_KEY_FILENAME : '/etc/webhook/certs/key.pem';
    let webserverCACertFilename = process.env.WEBSERVER_CA_CERT_FILENAME? process.env.WEBSERVER_CA_CERT_FILENAME : '/etc/webhook/certs/cert.pem';
    let baseUrls = (process.env.BASE_URLS? process.env.BASE_URLS : 'example.com').split(",");

    for (let i = 2; i < process.argv.length; i++) {
        switch (process.argv[i]) {
            case "-b":
                i++;
                if ((i+1) < process.argv.length) {
                    baseUrls = process.argv[i].split(",");
                }
                break;
            case "-c":
                i++;
                if ((i+1) < process.argv.length) {
                    webserverCACertFilename = process.argv[i];
                }
                break;
            case "-k":
                i++;
                if ((i+1) < process.argv.length) {
                    webserverPrivateKeyFilename = process.argv[i];
                }
                break;
            case "-t":
                testRun = true;
                break;
        }
    }

    return {
        privateKeyFilename: webserverPrivateKeyFilename,
        caCertFilename: webserverCACertFilename,
        testRun: testRun,
        baseUrls: baseUrls,
    }
}

/**
 * Builds and returns the HTTP response object.
 *
 * @param uid The UID from the request.
 * @param status The HTTP response status code to send.
 * @param allowed A boolean indicating whether the request is allowed.
 * @param message The message to return to the user.
 * @returns {{apiVersion: string, kind: string, response: {uid: *, allowed: *, status: {code: *, message: *}}}}
 */
function buildResponse(uid, status, allowed, message) {
    return {
        apiVersion: "admission.k8s.io/v1beta1",
        kind: "AdmissionReview",
        response: {
            uid: uid,
            allowed: allowed,
            status: {
                code: status,
                message: message
            }
        }
    };
}

/**
 * Fetches all Ingress resources in the cluster.
 *
 * @returns {Promise<Array<V1beta1Ingress>>}
 */
function fetchAllIngresses() {
    return k8sExtensionsApi.listIngressForAllNamespaces()
        .then(response => {
            return response.body.items;
        });
}

/**
 * Extracts all of the URLs (host+path) from each of the ingresses.
 *
 * @param ingresses The ingress resources to search.
 * @param skipIngress An optional ingress object to skip, used to skip itself when updating an ingress.
 */
function extractAllUrls(ingresses, skipIngress) {
    let allUrls = {};
    ingresses.forEach(ingress => {
        try {
            if (!(skipIngress && ingress.metadata.name === skipIngress.name && ingress.metadata.namespace === skipIngress.namespace)) {
                let hostsAndPaths = extractHostsAndPaths(ingress);

                if (hostsAndPaths) {
                    hostsAndPaths.forEach(host => {
                        host.paths.forEach(path => {
                            allUrls[(host.host + path)] = {
                                ingressName: host.name,
                                ingressNamespace: host.namespace
                            };
                        });
                    });
                }
            }
        } catch (error) {
            console.error(error);
        }
    });
    return allUrls;
}

/**
 * Extracts all of the TLS hosts from the given ingress resource.
 *
 * @param ingress The ingress resource to search for TLS hosts.
 * @returns {[]}
 */
function extractTlsHosts(ingress) {
    let hosts = [];
    if (ingress.spec.tls) {
        ingress.spec.tls.forEach(tls => {
            tls.hosts.forEach(host => hosts.push(host));
        });
    }
    return hosts;
}

/**
 * Extracts the namespace, hosts, and paths from the given ingress resource.
 *
 * @param ingress The ingress resource to search for host+path combinations.
 * @returns {[]}
 */
function extractHostsAndPaths(ingress) {
    let hosts = [];
    try {
        if (ingress.spec.rules) {
            ingress.spec.rules.forEach(rule => {
                let host = rule.host;
                let paths = [];
                if (rule.http.paths) {
                    rule.http.paths.forEach(path => {
                        paths.push(path.path);
                    });
                }
                hosts.push({
                    host: host,
                    paths: paths,
                    name: ingress.metadata.name,
                    namespace: ingress.metadata.namespace
                });
            });
        }
    } catch (error) {
        console.error(error);
    }
    return hosts;
}

/**
 * Parses the request body.  If the request is an UPDATE, returns the ingress being updated as skipIngress.  If the
 * request is a DELETE, returns 'true' for skipTests; otherwise skipTests will be false.
 *
 * @param body The HTTP body to be parsed.
 * @returns {{admissionRequest: *, skipIngress: *, skipTests: *}}
 */
function parseRequest(body) {
    let admissionRequest = JSON.parse(body);
    let skipIngress;
    let skipTests = false;
    if (admissionRequest.request.operation === "UPDATE") {
        skipIngress = {
            name: admissionRequest.request.name,
            namespace: admissionRequest.request.namespace
        }
    } else if (admissionRequest.request.operation === "DELETE") {
        skipTests = true;
    }

    return { admissionRequest, skipIngress, skipTests };
}

/**
 * Given an array of all ingresses in the cluster and an array of host+path combinations for the incoming ingress,
 * tests whether all host+path combinations are not already defined in existing ingress resources.
 *
 * @param ingresses An array of all ingress resources currently in the cluster.
 * @param testHostsAndPaths An array of host+path combinations for the new ingress being added/updated
 * @param skipIngress If the operation is an update, this is the ingress resource being updated, so that the tests
 * can skip the updated ingress.
 * @returns {[]}
 */
function testUrls(ingresses, testHostsAndPaths, skipIngress) {
    let failures = [];
    let allUrls = extractAllUrls(ingresses, skipIngress);
    testHostsAndPaths.forEach(host => {
        host.paths.forEach(path => {
            let testUrl = host.host + path;
            if (allUrls[testUrl]) {
                failures.push({
                    hostAndPath: testUrl,
                    ingressName: allUrls[testUrl].ingressName,
                    ingressNamespace: allUrls[testUrl].ingressNamespace
                })
            }
        });
    });

    return failures;
}

function testTlsHosts(tlsHosts, hostsAndPaths) {
    let hosts = new Set();
    hostsAndPaths.forEach(host => hosts.add(host.host));

    let failures = [];
    try {
        tlsHosts.forEach(host => {
            if (!hosts.has(host)) {
                failures.push(host);
            }
        });
    } catch (error) {
        console.error(error);
    }

    return failures;
}

function testBaseUrls(hosts, tlsHosts, baseUrls) {
    let failures = [];
    try {
        hosts.forEach(host => {
            let found = false;
            baseUrls.forEach(baseUrl => {
                if (host.host.endsWith(baseUrl)) {
                    found = true;
                }
            });
            if (!found) {
                failures.push(host.host);
            }
        });
        tlsHosts.forEach(host => {
            let found = false;
            baseUrls.forEach(baseUrl => {
                if (host.endsWith(baseUrl)) {
                    found = true;
                }
            });
            if (!found) {
                failures.push(host);
            }
        })
    } catch (error) {
        console.error(error);
    }

    return failures;
}

function testRequest(body, options) {
    let { admissionRequest, skipIngress, skipTests } = parseRequest(body);

    let ingress = admissionRequest.request.object;
    let tlsHosts = extractTlsHosts(ingress);
    let hostsAndPaths = extractHostsAndPaths(ingress);

    let baseUrlFailures = skipTests? [] : testBaseUrls(hostsAndPaths, tlsHosts, options.baseUrls);
    let tlsFailures = skipTests? [] : testTlsHosts(tlsHosts, hostsAndPaths);
    let allowed = true;

    console.log(`baseUrlFailures: ${JSON.stringify(baseUrlFailures, null, 2)}`);
    console.log(`tlsFailures: ${JSON.stringify(tlsFailures, null, 2)}`);
    return fetchAllIngresses().then(ingresses => {
        let urlFailures = skipTests ? [] : testUrls(ingresses, hostsAndPaths, skipIngress);

        if (urlFailures.length === 0 && tlsFailures.length === 0 && baseUrlFailures.length === 0) {
            return "";
        } else {
            console.log(`URL test failure with ingress: ${ingress.metadata.name}`);
            allowed = false;

            let message = "";
            if (baseUrlFailures.length > 0) {
                message += "\nBad base URL: " + JSON.stringify(baseUrlFailures, null, 2)
            }
            if (tlsFailures.length > 0) {
                message += "\nBad TLS Hosts: " + JSON.stringify(tlsFailures, null, 2);
            }
            if (urlFailures.length > 0) {
                message += "\nHost+path is not unique; all host+path combinations must be unique in the cluster.\nDuplicate Ingress hosts+paths: " + JSON.stringify(urlFailures, null, 2);
            }
            return message;
        }
    }).then(message => {
        let admissionReview = buildResponse(admissionRequest.uid, 200, allowed, message);
        return { status: 200, response: admissionReview };
    }).catch(error => {
        console.error("UNKNOWN ERROR");
        console.error(error);
        let admissionReview = buildResponse(admissionRequest.uid, 500, false, JSON.stringify(error));
        return { status: 500, response: admissionReview };
    });
}

let options = parseArgs();
let filteredOptions = {...options};
filteredOptions.password = '-redacted-';
console.log(`options: ${JSON.stringify(filteredOptions, null, 2)}`);

const httpsOptions = {
    key: fs.readFileSync(options.privateKeyFilename),
    cert: fs.readFileSync(options.caCertFilename)
};

https.createServer(httpsOptions, (request, response) => {
    if (request.method === 'POST') {
        let body = '';
        request.on('data', function(data) {
            body += data;
        });
        request.on('end', function() {
            testRequest(body, options).then(respValues => {
                if (respValues.allowed) {

                }
                response.writeHead(respValues.status,{'Content-Type': 'application/json'});
                response.end(JSON.stringify(respValues.response));
            });
        })
    } else {
        response.writeHead(200, "Must use POST http method", {
            'content-type': 'text/plain'
        });
        response.end('incorrect HTTP method');
    }
}).listen(port);
console.log(`Listening at https://${host}:${port}`);
