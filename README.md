To install, from this directory type:

./bin/install.sh

This script will:

1. Create a service account named "url-checker"
2. Deploy a clusterrole + clusterrolebinding for the new service account
3. Generate a signed TLS certificate as a secret
named "url-checker-certs" in the "kube-system" namespace
4. Deploy the url-checker deployment/service as a helm chart
5. Set the certificate authority cert into the validating webhook configuraiton yaml
6. Deploy the validating webhook configuration
