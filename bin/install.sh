#!/bin/bash

set -e

kubectl create serviceaccount url-checker -n kube-system

kubectl apply -f ../deployment/clusterrole.yaml
kubectl apply -f ../deployment/clusterrolebinding.yaml

./webhook-create-signed-cert.sh

helm install --name url-checker --namespace kube-system ../chart/url-checker/.

sleep 60

cat ../deployment/validatingwebhook.yaml | ./webhook-patch-ca-bundle.sh | kubectl apply -f -
